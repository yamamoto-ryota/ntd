package com.june.ntd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages={"com.june.ntd.controller"})
@EntityScan("com.june.ntd.entity")
@EnableJpaRepositories("com.june.ntd.repository")
public class NtdApplication {

	public static void main(String[] args) {
		SpringApplication.run(NtdApplication.class, args);
	}

}
