package com.june.ntd.entity;

import java.util.ArrayList;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer"})
@Table(name = "lists")
public class List {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "name",nullable= false)
	private String name;

	@Column(name = "board_id",nullable= false)
	private Integer boardId;

	@Column(name = "created_date",updatable = false)
	private Date createdDate;

	@Column(name = "updated_date")
	private Date updatedDate;

	@Column(name = "sequence_num", nullable = false)
	private Integer sequenceNum;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval=true, fetch = FetchType.EAGER)
	@JoinColumn(name = "list_id",nullable=true,updatable=false,insertable=false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@OrderBy(value = "sequence_num asc")
	private java.util.List<Task> tasks = new ArrayList<>();

	public java.util.List<Task> getTasks() {
		return tasks;
	}

	public void setTasks(java.util.List<Task> tasks) {
		this.tasks = tasks;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getBoardId() {
		return boardId;
	}

	public void setBoardId(Integer boardId) {
		this.boardId = boardId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Integer getSequenceNum() {
		return sequenceNum;
	}

	public void setSequenceNum(Integer sequenceNum) {
		this.sequenceNum = sequenceNum;
	}

	@PrePersist
	void preInsert() {
		if (this.createdDate == null)
			this.createdDate = new Date(System.currentTimeMillis());

		if (this.updatedDate == null)
			this.updatedDate = new java.util.Date();
	}

	@PreUpdate
	void preUpdate() {
		if(this.updatedDate == null)
			this.updatedDate = new java.util.Date();
	}

}
