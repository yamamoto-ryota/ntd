package com.june.ntd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.june.ntd.entity.Comment;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer> {

	List<Comment> findCommentByUserId(Integer userId);

	List<Comment> findCommentByTaskId(Integer taskId);


}
