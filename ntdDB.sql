create table boards(
    id serial not null,
    name varchar(30),
    user_id integer not null,
    created_date timestamp not null default current_timestamp,
    updated_date timestamp not null default current_timestamp,
    primary key (id)
);

create table tasks(
    id serial not null,
    task varchar(30),
    user_id integer not null,
    list_id integer not null,
    detail text,
    completed_date timestamp,
    due_date timestamp,
    created_date timestamp not null default current_timestamp,
    updated_date timestamp not null default current_timestamp,
    primary key (id)
);

create table lists(
    id serial not null,
    name varchar(30) not null,
    board_id integer not null,
    created_date timestamp not null default current_timestamp,
    updated_date timestamp not null default current_timestamp,
    primary key (id)
);

create table users(
    id serial not null,
    account varchar(30) not null,
    name varchar(30) not null,
    sns_id bigint unique not null,
    password varchar(255) not null,
    email varchar(50) unique not null,
    introduction text,
    created_date timestamp not null default current_timestamp,
    updated_date timestamp not null default current_timestamp,
    primary key (id)
);

create table comments(
    id serial not null,
    comment text not null,
    user_id integer not null,
    task_id integer not null,
    created_date timestamp not null default current_timestamp,
    updated_date timestamp not null default current_timestamp,
    primary key (id)
);


insert into boards (
    name,
    user_id
) values (
    'First Board Dump',
    1
),(
    'Second Board Dump',
    2
),(
    'Third Board Dump',
    3
);

insert into tasks 
    (task,user_id,list_id, detail,completed_date,due_date)
values
    ('I have to study React.js',1,1,'first detail dump','2020-10-10','2020-09-10'),
    ('I have to study Spring boot',1,1,'second detail dump','2020-10-11','2020-09-11'),
    ('To study Restful Apps',1,1,'third detail dump','2020-10-12','2020-09-12');
    
insert into lists (
    name,
    board_id
)values
('Wating Tasks',1),
('Working Tasks',1),
('Completed Tasks',1);

insert into users(
    account,
    name,
    sns_id,
    password,
    email,
    introduction
)values(
    '@accountDumpUno',
    'nameUno',
    123456789,
    'password',
    'email_dump@m.heisha.jp',
    'Hello. My name is nameUno'
),
(
    '@accountDumpDos',
    'nameDos',
    222222222,
    'password',
    'email_dump_uno@m.heisha.jp',
    'Hello. My name is nameDos'
),
(
    '@accountDumpTres',
    'nameTres',
    333333333,
    'password',
    'email_dump_tres@m.heisha.jp',
    'Hello. My name is nameTres'
);

insert into comments(
    comment,
    user_id,
    task_id
) values
('Uno comment',1,1),
('Dos comment',2,1),
('Tres comment',1,2);


ALTER TABLE users ALTER COLUMN sns_id DROP NOT NULL;
ALTER TABLE users ALTER COLUMN password DROP NOT NULL;
ALTER TABLE users ALTER COLUMN account DROP NOT NULL;
ALTER TABLE users ALTER COLUMN sns_id TYPE VARCHAR(30);

ALTER TABLE tasks ADD COLUMN color VARCHAR(20);
ALTER TABLE tasks ADD COLUMN sequence_num INTEGER;
ALTER TABLE lists ADD COLUMN sequence_num INTEGER;

UPDATE tasks SET sequence_num = 0;
UPDATE lists SET sequence_num = 0;

ALTER TABLE tasks ALTER COLUMN sequence_num SET NOT NULL;
ALTER TABLE lists ALTER COLUMN sequence_num SET NOT NULL;
