package com.june.ntd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ListRepository extends JpaRepository<com.june.ntd.entity.List, Integer> {

	List<com.june.ntd.entity.List> findListByBoardIdOrderBySequenceNumAsc(Integer boardId);

}
