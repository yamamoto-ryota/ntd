package com.june.ntd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.june.ntd.entity.Task;

@Repository
public interface TaskRepository extends JpaRepository<Task, Integer> {

	public List<Task> findTaskByUserId(Integer userId);
	public List<Task> findTaskByListIdOrderBySequenceNumAsc(Integer listId);

}
