package com.june.ntd.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer"})
@Table(name = "tasks")
public class Task {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "task",nullable= false)
	private String task;

	@Column(name = "user_id",nullable= false)
	private Integer userId;

	@Column(name = "list_id",nullable= false)
	private Integer listId;

	@Column(name = "detail")
	private String detail;

	@Column(name = "completed_date")
	private Date completedDate;

	@Column(name = "due_date")
	private Date dueDate;

	@Column(name = "created_date",updatable = false)
	private Date createdDate;

	@Column(name = "updated_date")
	private Date updatedDate;

	@Column(name = "color")
	private String color;

	@Column(name = "sequence_num", nullable=false)
	private Integer sequenceNum;


	/*
	@ManyToOne
	@JoinTable(name = "list")*/
//	private com.june.ntd.entity.List list;
//
//	public com.june.ntd.entity.List getList() {
//		return list;
//	}
//
//	public void setList(com.june.ntd.entity.List list) {
//		this.list = list;
//	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTask() {
		return task;
	}

	public void setTask(String task) {
		this.task = task;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getListId() {
		return listId;
	}

	public void setListId(Integer listId) {
		this.listId = listId;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public Date getCompletedDate() {
		return completedDate;
	}

	public void setCompletedDate(Date completedDate) {
		this.completedDate = completedDate;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Integer getSequenceNum() {
		return sequenceNum;
	}

	public void setSequenceNum(Integer sequenceNum) {
		this.sequenceNum = sequenceNum;
	}



	@PrePersist
	void preInsert() {
		if (this.createdDate == null)
			this.createdDate = new Date(System.currentTimeMillis());

		if (this.updatedDate == null)
			this.updatedDate = new java.util.Date();
	}

	@PreUpdate
	void preUpdate() {
		if(this.updatedDate == null)
			this.updatedDate = new java.util.Date();
	}
}
