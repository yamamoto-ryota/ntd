package com.june.ntd.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.june.ntd.entity.Board;
import com.june.ntd.entity.Comment;
import com.june.ntd.entity.Task;
import com.june.ntd.entity.User;
import com.june.ntd.repository.BoardRepository;
import com.june.ntd.repository.CommentRepository;
import com.june.ntd.repository.ListRepository;
import com.june.ntd.repository.TaskRepository;
import com.june.ntd.repository.UserRepository;
import com.june.ntd.request.BoardRequest;
import com.june.ntd.request.CommentRequest;
import com.june.ntd.request.ListRequest;
import com.june.ntd.request.TaskRequest;
import com.june.ntd.request.UserRequest;

@RestController
@RequestMapping("/ntd")
@CrossOrigin("*")
public class NtdController {

	@Autowired
	private final BoardRepository boardRepository;

	@Autowired
	private final UserRepository userRepository;

	@Autowired
	private final TaskRepository taskRepository;

	@Autowired
	private final ListRepository listRepository;

	@Autowired
	private final CommentRepository commentRepository;

	public NtdController(BoardRepository boardRepository,
			UserRepository userRepository,
			CommentRepository commentRepository,
			ListRepository listRepository,
			TaskRepository taskRepository) {

		this.boardRepository = boardRepository;
		this.userRepository = userRepository;
		this.commentRepository = commentRepository;
		this.listRepository = listRepository;
		this.taskRepository = taskRepository;

	}
	
	@GetMapping("")
	public String getHelloWorld() {
		return "Hello World";
	}

	/*GET*/
	@GetMapping("/board/getAll")
	public List<Board> getBoardAll() {
		return boardRepository.findAll();
	}

	@GetMapping("/board/get/{id}")
	public Board getBoardById(@PathVariable("id") Integer id) {
		return boardRepository.getOne(id);
	}

	@GetMapping("/board/get/byUserId/{userId}")
	public List<Board> getBoardByUserId(@PathVariable("userId") Integer userId){
		return boardRepository.findBoardByUserIdOrderByUpdatedDateAsc(userId);
	}

	/* POST */
	@PostMapping("/board/set")
	public Board save(@RequestBody BoardRequest request) {
			Board entity = new Board();
			entity.setName(request.getName());
			entity.setUserId(request.getUserId());
			return boardRepository.save(entity);
	}

	/*PUT*/
	@PutMapping("/board/update/{id}")
	public Board update(@RequestBody BoardRequest request,
			@PathVariable("id") Integer id) {

			Board entity = new Board();
			entity.setName(request.getName());
			entity.setUserId(request.getUserId());
			entity.setId(id);
			return boardRepository.saveAndFlush(entity);
			}

	/*DELETE */
	@DeleteMapping("/board/delete/{id}")
	public void deleteBoard(@PathVariable("id") Integer id ) {
		boardRepository.deleteById(id);
	}

	/*GET*/
	@GetMapping("/user/getAll")
	public List<com.june.ntd.entity.User> getUserAll() {
		return userRepository.findAll();
	}

	@GetMapping("/user/get/{id}")
	public User getUserById(@PathVariable("id") Integer id) {

		return userRepository.getOne(id);
	}


	@GetMapping("/user/get/email")
	public User getUserByForm(
			@RequestParam(value = "email", required = false) String email){
		return userRepository.findByEmail(email);
	}

	@GetMapping("/user/get/byEmail/{email}")
	public User getUserbyEmail(@PathVariable("email") String email) {
		return userRepository.findByEmail(email);
	}

	@GetMapping("/user/get/login")
	public User getLoginUser(
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "password", required=false)String password){
		return userRepository.findByPasswordAndEmail(password,email);
	}

	@GetMapping("/user/get/login/{email}/{password}")
	public User getLoginUserVerPath(
			@PathVariable("email") String email,
			@PathVariable("password") String password) {
		return userRepository.findByPasswordAndEmail(password,email);
	}

	@GetMapping("/user/get/socialLogin/{email}/{snsId}")
	public User getSocialLoginUser(
			@PathVariable("email") String email,
			@PathVariable("snsId") String snsId) {
		return userRepository.findByEmailAndSnsId(email,snsId);
	}

	/* POST */
	@PostMapping("/user/set")
	public User save(@RequestBody UserRequest request) {
			User entity = new User();
			entity.setAccount(request.getAccount());
			entity.setName(request.getName());
			entity.setSnsId(request.getSnsId());
			entity.setPassword(request.getPassword());
			entity.setEmail(request.getEmail());
			entity.setIntroduction(request.getIntroduction());
			return userRepository.save(entity);
	}

	/*PUT*/
	@PutMapping("/user/update/{id}")
	public User update(@RequestBody UserRequest request,
			         @PathVariable("id") Integer id) {
			User entity = new User();
			entity.setAccount(request.getAccount());
			entity.setName(request.getName());
			entity.setSnsId(request.getSnsId());
			entity.setPassword(request.getPassword());
			entity.setEmail(request.getEmail());
			entity.setIntroduction(request.getIntroduction());
			entity.setId(id);
			return userRepository.saveAndFlush(entity);
	}

	/*DELETE */
	@DeleteMapping("/user/delete/{id}")
	public void deleteuser(@PathVariable("id") Integer id ) {
		userRepository.deleteById(id);
	}

	@GetMapping("/task/getAll")
	public List<Task> getTaskAll() {
		return taskRepository.findAll();
	}

	@GetMapping("/task/get/{id}")
	public Task gettaskById(@PathVariable("id") Integer id) {
		return taskRepository.getOne(id);
	}

	@GetMapping("/task/get/byUserId/{userId}")
	public List<Task> getTaskByUserId(@PathVariable("userId") Integer userId) {
		return taskRepository.findTaskByUserId(userId);
	}

	@GetMapping("/task/get/byListId/{listId}")
	public List<Task> getTaskByListId(@PathVariable("listId") Integer listId) {
		//Sort sort = new Sort(Direction.DESC, "sequenceNum");
		return taskRepository.findTaskByListIdOrderBySequenceNumAsc(listId);
	}

	/* POST */
	@PostMapping("/task/set")
	public Task save(@RequestBody TaskRequest request) {
			Task entity = new Task();
			entity.setTask(request.getTask());
			entity.setUserId(request.getUserId());
			entity.setListId(request.getListId());
			entity.setDetail(request.getDetail());
			entity.setDueDate(request.getDueDate());
			entity.setCompletedDate(request.getCompletedDate());
			entity.setSequenceNum(request.getSequenceNum());
			return taskRepository.save(entity);
	}

	/*PUT*/
	@PutMapping("/task/update/{id}")
	public Task update(@RequestBody TaskRequest request,
			           @PathVariable("id") Integer id) {
			Task entity = new Task();
			entity.setTask(request.getTask());
			entity.setUserId(request.getUserId());
			entity.setListId(request.getListId());
			entity.setDetail(request.getDetail());
			entity.setDueDate(request.getDueDate());
			entity.setCompletedDate(request.getCompletedDate());
			entity.setId(id);
			entity.setSequenceNum(request.getSequenceNum());
			return taskRepository.saveAndFlush(entity);
	}

	/*DELETE */
	@DeleteMapping("/task/delete/{id}")
	public void deleteTask(@PathVariable("id") Integer id ) {
		taskRepository.deleteById(id);
	}

	/*LIST*/

	@GetMapping("/list/getAll")
	public List<com.june.ntd.entity.List> getListAll() {
		return listRepository.findAll();
	}

	@GetMapping("/list/get/{id}")
	public com.june.ntd.entity.List getListById(@PathVariable("id") Integer id) {
		return listRepository.getOne(id);
	}

	@GetMapping("/list/get/byBoardId/{boardId}")
	public List<com.june.ntd.entity.List> getListByBoardId(@PathVariable("boardId") Integer boardId) {
		return listRepository.findListByBoardIdOrderBySequenceNumAsc(boardId);
	}

	/* POST */
	@PostMapping("/list/set")
	public com.june.ntd.entity.List save(@RequestBody ListRequest request) {
			com.june.ntd.entity.List entity = new com.june.ntd.entity.List();
			entity.setName(request.getName());
			entity.setBoardId(request.getBoardId());
			entity.setSequenceNum(request.getSequenceNum());
			return listRepository.save(entity);
	}

	/*PUT*/
	@PutMapping("/list/update/{id}")
	public com.june.ntd.entity.List update(@RequestBody ListRequest request,
			                               @PathVariable("id") Integer id) {
			com.june.ntd.entity.List entity = new com.june.ntd.entity.List();
			entity.setId(id);
			entity.setName(request.getName());
			entity.setBoardId(request.getBoardId());
			entity.setSequenceNum(request.getSequenceNum());
			entity.setTasks( listRepository.getOne(id).getTasks());//結合されたタスクのテーブル情報
			return listRepository.save(entity);
	}

	/*DELETE */
	@DeleteMapping("/list/delete/{id}")
	public void deleteList(@PathVariable("id") Integer id ) {
		listRepository.deleteById(id);
	}

	@GetMapping("/comment/getAll")
	public List<Comment> getCommentAll() {
		return commentRepository.findAll();
	}

	@GetMapping("/comment/get/{id}")
	public Comment getCommentById(@PathVariable("id") Integer id) {
		return commentRepository.getOne(id);
	}

	@GetMapping("/comment/get/byUserId/{userId}")
	public List<Comment> getCommentByUserId(@PathVariable("userId") Integer userId){
		return commentRepository.findCommentByUserId(userId);
	}

	@GetMapping("/comment/get/byTaskId/{taskId}")
	public List<Comment> getCommentByTaskId(@PathVariable("taskId") Integer taskId){
		return commentRepository.findCommentByTaskId(taskId);
	}

	/* POST */
	@PostMapping("/comment/set")
	public Comment save(@RequestBody CommentRequest request) {
			Comment entity = new Comment();
			entity.setComment(request.getComment());
			entity.setUserId(request.getUserId());
			entity.setTaskId(request.getTaskId());
			return commentRepository.save(entity);
	}

	/*PUT*/
	@PutMapping("/comment/update/{id}")
	public Comment update(@RequestBody CommentRequest request,
			              @PathVariable("id") Integer id) {
			Comment entity = new Comment();
			entity.setComment(request.getComment());
			entity.setUserId(request.getUserId());
			entity.setTaskId(request.getTaskId());
			return commentRepository.save(entity);
	}

	/*DELETE */
	@DeleteMapping("/comment/delete/{id}")
	public void deleteComment(@PathVariable("id") Integer id ) {
		commentRepository.deleteById(id);
	}

}
