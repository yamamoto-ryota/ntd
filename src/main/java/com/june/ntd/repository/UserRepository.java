package com.june.ntd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.june.ntd.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

public User findByEmail( String email);
public User findByPasswordAndEmail(String password, String email);
public User findByEmailAndSnsId(String email, String snsId);

}