package com.june.ntd.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties({ "hibernateLazyInitializer" })
@Table(name = "boards")
public class Board {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "name",nullable= false)
	private String name;

	@Column(name = "user_id",nullable= false)
	private Integer userId;

	@Column(name = "created_date",updatable = false, columnDefinition = "default 'CURRENT_TIMESTAMP'")
	private Date createdDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_date")
	private java.util.Date updatedDate;
/*
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval=true, fetch = FetchType.EAGER)
	@JoinColumn(name = "borad_id",nullable=true,updatable=false,insertable=false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@OrderBy(value = "sequence_num desc")
	private java.util.List<com.june.ntd.entity.List> lists = new ArrayList<>();

	public java.util.List<com.june.ntd.entity.List> getLists() {
		return lists;
	}

	public void setTasks(java.util.List<com.june.ntd.entity.List> lists) {
		this.lists = lists;
	}
*/

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public java.util.Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(java.util.Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@PrePersist
	void preInsert() {
		if (this.createdDate == null)
			this.createdDate = new Date(System.currentTimeMillis());

		if (this.updatedDate == null)
			this.updatedDate = new java.util.Date();
	}

	@PreUpdate
	void preUpdate() {
		if(this.updatedDate == null)
			this.updatedDate = new java.util.Date();
	}

}
